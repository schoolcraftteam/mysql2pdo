<?php

namespace Schoolcraftteam;

/**
 * Helper class to emulate last insert id.
 * Setter should be called on every query.
 * Getter will return static class var.
 */
class PdoHelper
{
    
    /**
     * statically store last insert id
     *
     * @var integer
     */
    private static $lastInsertId = 0;
    
    /**
     * statically store error info
     */
    private static $errorInfo = null;
    
    /**
     * Getter for DB username
     *
     * @return void
     */
    public static function getDatabaseUsername()
    {
        return $_ENV['DATABASE_USER'] ?? 
            $_ENV['DATABASE_USERNAME'] ?? 
            $_ENV['DB_USER'] ?? 
            $_ENV['DB_USERNAME'];
    }
    
    /**
     * Getter for DB password
     *
     * @return void
     */
    public static function getDatabasePassword()
    {
        return $_ENV['DATABASE_PASSWORD'] ?? 
            $_ENV['DATABASE_PW'] ?? 
            $_ENV['DB_PASSWORD'] ?? 
            $_ENV['DB_PW'];
    }
    
    /**
     * Getter for DB servername
     *
     * @return void
     */
    public static function getDatabaseServer()
    {
        return $_ENV['DATABASE_SERVER'] ?? 
            $_ENV['DATABASE_HOST'] ?? 
            $_ENV['DB_HOST'] ?? 
            $_ENV['DB_SERVER'];
    }
    
    /**
     * Getter for DB name
     *
     * @return void
     */
    public static function getDatabaseName()
    {
        return $_ENV['DATABASE_DBNAME'] ?? 
            $_ENV['DATABASE_NAME'] ?? 
            $_ENV['DATABASE_DBNAME'] ?? 
            $_ENV['DATABASE_DB'] ??
            $_ENV['DB_DBNAME'] ??
            $_ENV['DB_NAME'] ?? 
            $_ENV['DB_DBNAME'] ?? 
            $_ENV['DB_DATABASE'];
    }
    
    /**
     * statically set last insert id var
     *
     * @param integer $id
     * @return void
     */
    public static function setLastInsertId($id = 0)
    {
        self::$lastInsertId = $id ? $id : 0;
    }
    
    /**
     * statically get last insert id
     *
     * @return void
     */
    public static function getLastInsertId()
    {
        return self::$lastInsertId;
    }
    
    /**
     * statically set error info
     *
     * @param integer $id
     * @return void
     */
    public static function setErrorInfo($info = null)
    {
        self::$errorInfo = $info ? $info : null;
    }
    
    /**
     * statically get error info
     *
     * @return void
     */
    public static function getErrorInfo()
    {
        return var_dump(self::$errorInfo);
    }
    
}
